console.log("Hello Universe");

//What are conditional statements?
//Conditional statements allow us to control the flow of our program. It allows us to run a statement/instruction if a condition is met or run another separate instruction if otherwise.

let numA = -1;

if (numA > 0) {
	console.log("numA is Greater Than 0");
}

console.log(numA > 0);

if (numA < 0) {
	console.log("numA is Lesser Than 0");
}

console.log(numA < 0);

let numB = 1; 

if (numB < 0) {
	console.log("numB is Lesser Than 0");
}


if (numB > 0) {
	console.log("numB is greater Than 0");
}

if (false === false) {
	console.log("Hello");
}

if (true) {
	console.log("Konnichiwa");
}

let x = 20;
// x was Initialized with the value of 20
x = (x < 18) ? true : false;
// x value was overwrite with a Boolean Value since the value is a conditional.
console.log("Result of Ternary Operator: " + x);
// x Value is false , since the condition was a false (20 < 18)